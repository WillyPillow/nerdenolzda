Jekyll::Hooks.register :pages, :post_write do |document|
  dirName = document.site.source + "/_comments/" + document.name
  Dir.mkdir(dirName) unless File.exists?(dirName)
end

Jekyll::Hooks.register :posts, :post_write do |document|
  dirName = document.site.source + "/_comments/" + document.id.split("/")[-1]
  Dir.mkdir(dirName) unless File.exists?(dirName)
end
