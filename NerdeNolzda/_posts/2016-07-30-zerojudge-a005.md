---
layout: post
title: "ZeroJudge: a005 Eva 的回家作業"
date: 2016-07-30 09:58:37 +0800
moddate: 2016-08-09 17:43:19 +0800
tags: zh en bilingual zerojudge c competitive-coding
---

[ZeroJudge Link (Zh)](http://zerojudge.tw/ShowProblem?problemid=a004)

{% highlight C %}
#include <stdio.h>

main() {
  int year;
  while(scanf("%d", &year) != EOF) {
    printf("%s\n", (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) ? "閏年" : "平年");
  }
  return 0;
}
{% endhighlight %}

