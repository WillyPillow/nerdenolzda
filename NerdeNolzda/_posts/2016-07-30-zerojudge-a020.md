---
layout: post
title: "ZeroJudge: a020 身分證檢驗"
date: 2016-07-30 10:14:38 +0800
moddate: 2016-08-09 17:43:19 +0800
tags: en zh bilingual zerojudge c competitive-coding
---

用陣列存放 A~Z 對應之數字。
Use an array to determine what numbers A~Z correspond to.

[ZeroJudge Link (Zh)](http://zerojudge.tw/ShowProblem?problemid=a020)

{% highlight C %}
#include <stdio.h>

const char LETTERS[] = {10 ,11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21,
                      22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33};

main() {
  char str[100], chr[] = "X";
  while (scanf("%s", str) != EOF) {
    int i, res = LETTERS[str[0] - 'A'];
    res = res / 10 + (res % 10) * 9;
    for (i = 1; i < 9; i++) {
      chr[0] = str[i];
      res += atoi(chr) * (9 - i);
    }
    chr[0] = str[9];
    res += atoi(chr);
    printf("%s\n", res % 10 ? "fake" : "real");
  }
  return 0;
}
{% endhighlight %}

