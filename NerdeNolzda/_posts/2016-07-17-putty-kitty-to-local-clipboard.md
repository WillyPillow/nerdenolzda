---
layout: post
title: Putty/Kitty To Local Clipboard
date: 2016-07-17 13:41:39 +0800
moddate: 2016-08-09 17:43:19 +0800
tags: en putty kitty ssh windows
---

Once in a while, I need to use [Putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) on Windows to connect to a guest Linux installation in [VirtualBox](https://www.virtualbox.org/) for things that aren't compatible with Cygwin. However, a slight annoyance is that I can't copy, say, a whole file to the Windows clipboard.

After some research, I found [PuttyClip](http://www.dmst.aueb.gr/dds/sw/windows/puttyclip/), which is essentially a patch on Putty that adds an option to allow the remote to send [ANSI escape codes](https://en.wikipedia.org/wiki/ANSI_escape_code) (AUX port on/off) to modify the local clipboard.

However, the putty version provided is outdated, and does not support some new cipher standards. Shortly after, I realized the Putty fork I had been using, [KiTTY](http://www.9bis.net/kitty/?action=news&zone=en), has the patch included.

With the client set up properly, the only thing left is to write a script on the remote box that sends the ANSI codes.

{% highlight shell %}
#!/bin/sh
echo -ne '\e[5i'
cat $*
echo -ne '\e[4i'
{% endhighlight %}

Which can then be called by ```[scriptName] [textFile]``` or ```cat [textFile] | [scriptName]```. However, this does not work on [Tmux](https://tmux.github.io/). A solution is the following:

{% highlight shell %}
#!/bin/sh
echo -ne '\ePtmux;\e\e[5i\e\\'
cat $*
echo -ne '\ePtmux;\e\e[4i\e\\'
{% endhighlight %}

This creates some escape codes at the beginning of the content, which look positioning related. I have no idea on how to remove them, but manually deleting them (at least for my usage) isn't that much of a problem.

