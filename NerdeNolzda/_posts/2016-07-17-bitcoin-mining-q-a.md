---
layout: post
title: 比特幣挖礦問答 Bitcoin Mining Q/A
date: 2016-07-17 15:16:28 +0800
moddate: 2016-08-09 17:43:19 +0800
tags: en zh bilingual bitcoin mining cryptocurrency
---

這是我在這的第一篇中文文章。文中會附上英文翻譯，但不保證完全句句對應。

This is my first post involving Chinese here. English translations are included, but I don't guarantee that they correspond 100% to the Chinese version.

由於我上次的[這篇文章](/2016/07/12/simple-monero-mining-botnet-analysis.html)，我與 [code風景區](http://codingsimplifylife.blogspot.tw/) 的作者有了一小段關於比特幣挖礦的對話。

I was having a conversation with the author of [Code Scenic](http://codingsimplifylife.blogspot.tw/) regarding Bitcoin mining due to [my last post](/2016/07/12/simple-monero-mining-botnet-analysis.html).

精簡版的對話如下，以免任何人有類似的疑問。

A condensed version of the conversation is below, in case anyone has similar questions.

(W: WillyPillow, C: Author Of Code Scenic / code 風景區作者)

> W: 挖礦不太算科學運算...
>    Mining isn't exactly scientific calculations...
>    幾篇說明：
>    Some links:
>    <http://www.coindesk.com/information/how-bitcoin-mining-works/> (En)(英)
>    <http://scimonth.blogspot.tw/2014/01/blog-post_803.html> (Zh)(中)
>    不過下面這篇一開始寫得不錯，到「挖礦不環保」就已經歪掉了，然後越來越社會主義 & statism，建議那下面就別看了
>    However, while the latter is pretty well-written at the beginning, it started to turn in a different direction from "Mining is not eco-friendly". Then it started to get full of more and more socialism and statism. I recommend not to continue reading below that.
>    關於「不環保」的指控，詳見 <https://en.bitcoin.it/wiki/Help:FAQ#Is_it_not_a_waste_of_energy.3F>
>    Regarding accusions of not being eco-friendly, see <https://en.bitcoin.it/wiki/Help:FAQ#Is_it_not_a_waste_of_energy.3F>
>    另外，[Monero](https://getmonero.org/) 底層原理是 [CryptoNote](https://cryptonote.org/)，跟 Bitcoin 又有不小的不同
>    By the way, [Monero](https://getmonero.org/) is based on [CryptoNote](https://cryptonote.org/), which is also different from Bitcoin.

> C: 我是指幫忙某種科學上的運算計劃
>    I mean helping out on some scientific research projects.

> W: 我以為你的理解是像 [Folding@Home](https://folding.stanford.edu/) 那樣?
>    I thought you were thinking of something like [Folding@Home](https://folding.stanford.edu/)?
>    我以為你的意思是進行某種科學研究的運算，例如求質數、模擬蛋白質結構...etc
>    I thought you meant doing calculations about some scientific studies, like finding prime numbers, simulating protein structures ...etc.
>    但我的意思是一直 (類似暴力破解) 計算 hash 值，和科研沒有直接關係
>    But what I meant was keep caculating hashes, similar to brute forcing, and not directly related to studies.

> C: 計算 hash 值是為了什麼啊？
>    What is calculation the hashes for?

> W: 這叫 proof of work
>    This is called "proof of work".
>    基本上挖礦時，依照當時總挖礦運算能力，會決定一個 difficulty 值
>    Basically, while mining, a difficulty value will be decided based on the total processing power at that time.
>    然後大家要暴力破解出一個讓 hash < 該 difficulty 的 nonce
>    Then everyone needs to brute force a nonce that causes the hash to be smalled than the difficulty.
>    讓一個 block 產生的時間大約 10 分鐘
>    This makes the generation time of a block to be about 10 minutes.
>    除了限制 block 產生的速度以外，也有安全性的因素
>    Aside from limiting the block generation speed, there are security reasons involved.
>    首先，因為暴力破解涉及到機率，因此每次破解出來 (驗證 block) 的節點很可能是不一樣的。
>    Firstly, since brute forcing is related to probability, the node that sucesses in brute foring the block (thus verifying it) is very likely to be different each time.
>    再來，就要講到比特幣的重要核心：區塊鏈
>    Then, we need to mention the an essential core of Bitcoin: the blockchain.
>    每個 block 都包含前一個 block 的 hash ，因此，如果你要竄改之前 block 的資料，就必須重新產生該 block 之後的所有 block。這也代表著你必須擁有超過比特幣網路的 50% 運算力，而這不僅難如登天，更是容易被發現。
>    Every block contains the hash of the last block. Thus if you were to modify the data of a previous block, you would need to regenerate all blocks after that. This means that you need to possess at least 50% of the computing power on the Bitcoin network. This is not only nearly impossible, but easy to be discovered once attempted.

> C: 不知道我目前的猜測對不對: 挖礦是利用網路貨幣匯率的變化賺取價差？
>    I don't know if my current guess is correct: mining is utilizing the relative price change of Internet currencies to gain profit?
>    後半段大致看得懂，但前面我不知道為甚麼會出現「block」這個東西
>    I can roughly understand the last part, but I don't know why "block" appeared in the beginning.

> W: 不太是。基於上面所述，挖礦是一種驗證他人交易的機制。(畢竟比特幣是分散式的，不會有審核的(貪腐)中央機構)
>    Not really. Based on the above, mining is a method to verify others' transactions. After all, Bitcoin is distributed, and there is no (corrupt) central authority to verify them.
>    這當然不是慈善事業，比特幣網路會透過所謂的 block reward (顧名思義，是給第一個成功猜出 hash 者的獎賞) 和交易手續費 來獎賞礦工。
>    Of course, this is not charity work. The Bitcoin network rewards the miners by the so-called "block reward" (which, as per its name, is the reward for the first node to guess the hash) and transaction fees.
>    換言之，你的計算資源 (通常稱作 hashing power) 越多，平均來說搶到頭香的機率越大，賺的錢也越多。
>    In other words, the more computing power (usually referred to as "hashing power") you have, the bigger chance that you come in first you'll get, and the more money you'll make.
>    而 block 簡單來說就是一堆交易紀錄。驗證 block 也就等同於驗證了這些交易 (例如 沒有花超過錢包餘額 ...etc)。
>    And "blocks", in simple words, are a bunch of transaction records. Verifying blocks is equivalent to verifying these transactions (like not spending over your wallet balance ...etc).

> C: 可是為甚麼驗證這個東西要這麼複雜呀？不是直接 ```assert(money - cost >= 0)``` 之類的就好了嗎？
>    But why does verifying this needs to be so complicated? Why don't you do something like ```assert(money - cost >= 0)``` directly?

> W: 請仔細想想，這是一個分散式的系統，無法找到一個能夠完全信任的節點執行這件事。
>    Please think it over carefully. This is a distributed system, and you can not find a totally trustable node to do the work.
>    如果沒有 hash ，便代表惡意節點能夠一直廣播「嘿，我有很多錢喔」。
>    If there were no hashes, malicious nodes could keep broadcasting "Hey, I have tons of money!"
>    而有了挖礦這項機制，代表驗證、發表 block 需要實際資源。
>    Once you introduce the mining mechanism, verifying and publishing a block needs actual resources.
>    想像一下，如果寄 email 需要解 10 道數學題，那對於一般使用者來說，負擔不大，但對於垃圾信件發送者來說，由於必須寄送上千上萬封郵件，計算量就大了。
>    Imagine that you need to solve 10 equations each time you send an email. For normal users, the burden isn't that much. But for spammers, since they have to send hundreds of thousands of emails, the computations grows.
>    除此之外，挖礦的機率性也使驗證的工作依照運算能力分散到網路上各個節點。
>    Also, the probability of mining distributes the verifying work to nodes on the network based on their processing power.
>    此時你可能會想，會不會有人取得大量運算資源，壟斷驗證過程？這就是上述「擁有超過比特幣網路 50% 的運算力」的現象。
>    You might be wondering, what if a person obtained loads of computing resources and monopolize the verifying process? This is the "possessing at least 50% of the computing power of the Bitcoin network" mentioned above.
>    解決方法就是挖礦的 block reward。因為有經濟誘因，使得大家鑽研增加自己 hashing power 的方式 (ASIC)，讓運算力基數不斷上升，欲取得 50% 成為幾乎不可能之事。
>    The solution is the block reward of mining. By introducing economic incentives, everyone tries their best to increase their hashing power (like ASICs), making the base processing power go up so that obtaining 50% becomes nearly impossible.

> C: 所以這個機制就是讓偽造交易無法通過驗證，除非有足夠的計算資源？
>    So this mechanism makes malformed trasactions unable to pass the verification process, unless enough processing power is possessed?
>    難怪會有人說這個東西不環保
>    No wonder some say that it's not eco-friendly.

> W: 是的 (足夠 ~= 天文數字)
>    Yes. (Enough ~= A zillion)
>    對於環保，隨著大家換成用 ASIC 運算，性能/電力比會提高。
>    Regarding eco-friendliness, when everyone switches to ASICs, the performance/power consumption will increase.
>    除此之外，挖礦規模不可能無限上綱 (規模不經濟)，畢竟處理廢熱很辛苦，而電費也是要錢的
>    Besides, the scale of mining can not go up indefinitely. After all, solving the heat problem is hard, and electricity costs money too.
>    再加上運用像冰島這種 (聽說) 供過於求的再生能源，整體電力需求不一定會比傳統金流產業高
>    Paired with abdundant regenerable power like in Iceland (what I've heard), the whole power consumption may not be higher than that of the traditional financial industry.
>    個人認為，很大的不環保因素是在 (例如) 大陸。和當局關係良好者常常可以取得免費電力進行挖礦。看看他們建燃煤發電廠的速度
>    In my opinion, a lot of the un-eco-friendliness resides in mainland China, where those with ties with authorities are often able to obtain free electricity to mine. Just look at the speed they build coal-burning power plants.
>    因為它是一個分散式的貨幣，不會被任何政府控制，不會有亂印鈔票的問題、不會莫名其妙被沒收、不會一夕之間變成廢紙、較難以追蹤 ...etc，對它特別有興趣。
>    Because it's a distributed currency, which will not be controlled by any government, will not be fall under the problem of printing cash as they like, will not be confiscated with no reason, will not turn into garbage in one night, is harder to trace ...etc, I'm especially interested in it.
>    這便有點像黃金的穩定性，但同時又擁有信用卡的方便性。
>    This is a bit like the stability of gold, but at the same time it possesses the convenience of credit cards.
>    這也跟政治有不小的關係。基本上我支持 [Agorism](https://en.wikipedia.org/wiki/Agorism) ，也就是以自由市場的方式，逐漸減輕政府的權力與影響力
>    This is also related to politics. Basically, I support [Agorism](https://en.wikipedia.org/wiki/Agorism), using the free market to gradually decrease the power and influence of governments.

