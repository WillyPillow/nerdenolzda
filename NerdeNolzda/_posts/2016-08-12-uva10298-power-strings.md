---
layout: post
title: "UVa10298: Power Strings"
date: 2016-08-12 06:19:02 +0800
moddate: 2016-11-05 21:57:19 +0800
tags: en zh bilingual cpp UVa competitive-coding
---

這題和 [UVa 455](/2016/08/12/uva455-periodic-strings.html) 幾乎一模一樣，但測資大上了不少，若暴力破解可能會逾時，需要更有效率的演算法。想像一個字串 `abcabcabc`。我們可以將它寫兩遍，成為 `abcabcabcabcabcabc`。此時，原字串便會如下圖成為新字串的 substring，其間隔便是周期。一般會實作 KMP ([Link 1](http://www.csie.ntnu.edu.tw/~u91029/StringMatching.html) [Link 2](https://www.ptt.cc/bbs/b99902HW/M.1300796484.A.EE1.html))，但此題用 strstr 即能達到不錯的效果。（GCC strstr 實作似乎使用 [Two Way Algorithm (En)](http://www-igm.univ-mlv.fr/~lecroq/string/node26.html))
This problem is almost identical to [UVa 455](/2016/08/12/uva455-periodic-strings.html), albeit with a bigger input. Thus, it might timeout if you simply do brute force, and we need a more efficient algorithm. Imagine a repeating string `abcabcabc`. We can repeat it after itself, making `abcabcabcabcabcabc`. Now, as the graph below shows, the original string becomes a substring of the new string, and the intervals are equal to the repetition period. Normally, [KMP](https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm) is used, but in this case, strstr works quite well too. (The GCC strstr implementation seems to utilize [Two Way Algorithm](http://www-igm.univ-mlv.fr/~lecroq/string/node26.html))

```
abcabcabcabcabcabc
abcabcabc
   abcabcabc
      abcabcabc
         abcabcabc
```

[UVa Link](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1239)

{% highlight C++ %}
#include <iostream>
#include <cstring>

char str[1000001];
char str2[2000002];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  while (std::cin >> str && str[0] != '.') {
    int len = strlen(str);
    sprintf(str2, "%s%s", str + 1, str);
    char *search = strstr(str2, str);
    int period = search - str2;
    std::cout << len / (period + 1) << '\n';
  }
}
{% endhighlight %}

