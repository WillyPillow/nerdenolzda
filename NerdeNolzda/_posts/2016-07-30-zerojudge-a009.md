---
layout: post
title: "ZeroJudge: a009 解碼器"
date: 2016-07-30 10:04:35 +0800
moddate: 2016-08-09 17:43:19 +0800
tags: en zh bilingual zerojudge c competitive-coding
---

K = 7

[ZeroJudge Link (Zh)](http://zerojudge.tw/ShowProblem?problemid=a009)

{% highlight C %}
#include <stdio.h>

main() {
  char str[100];
  while(scanf("%s", str) != EOF) {
    int i = 0;
    while(str[i]) {
      str[i++] -=7;
    }
    printf("%s\n", str);
  }
  return 0;
}
{% endhighlight %}

