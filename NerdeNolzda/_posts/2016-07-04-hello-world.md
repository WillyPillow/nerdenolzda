---
layout: post
title: Hello World & Technical Details
date: 2016-07-04 20:23:37 +0800
moddate: 2016-08-09 17:43:19 +0800
tags: en
---

Hi guys. As you can see, this is my first post here. ~~I'm so excited!~~ I've always wanted to have a place to write about stuff I've been fiddling with, which I've actually tried once, but backed out in a few weeks.

Anyway, in this article, I'll talk about what tools/services I used to build this site.

## Jekyll
As someone who likes customization and simplicity, I didn't want to use bloaty blogging engines like WordPress. Thus, I went with [Jekyll](https://jekyllrb.com/), an static page generator. In essence, you modify a few configuration files, include some plugins that you need, write your articles in Markdown, and you get a set of HTML files that you can put on any web server, no matter PHP or the like is supported or not, making it trivial to migrate from one hosting to another. This is great for me since I will put this site on [Freenet](https://freenetproject.org/) in the near future. Having no server side scripts also means that it puts less burden on the hosting servers, allowing more concurrent viewers. In addition, Markdown is awesome to use, especially for text and code oriented sites like this one. Another great thing is that I can easily edit the site offline if I ever have the need.

Currently, the third party plugins that I use are:

* [jekyll-tagging](https://github.com/pattex/jekyll-tagging)
* [jekyll-sitemap](https://github.com/jekyll/jekyll-sitemap)
* [jekyll-static-comments](https://github.com/binfalse/jekyll-static-comments)

A git repository of this site can be found [here](https://bitbucket.org/WillyPillow/nerdenolzda).

## Python (and Ruby)
I wanted to have a commenting feature on the site, but didn't want to use Disqus since it uses rather heavy client side Javascript. This would create problems if I wanted to put the site on Freenet. Not only that, but I usually dislike sites that are unusable without Javascript enabled. Server side scripts are also frowned at, since it kinda beats the purpose of having static pages. In the end, as mentioned above, I installed jekyll-static-comments, and wrote a simple Python script that fetches email through IMAP and turn them into comments, together with a plugin that creates comment subdirectories based on the post ID. As a side note, the plugin was prone to XSS attacks, so I hacked together a plugin that calls [sanitize](https://github.com/rgrove/sanitize) to strip potentially malicious tags.

The scripts and plugins mentioned above can be found at [jekyll-static-comments-helper](https://bitbucket.org/WillyPillow/jekyll-static-comments-helper).

## Gulp
I use a self-modified version of [jekyll-gulp-optimizations](https://github.com/dimitri-koenig/jekyll-gulp-optimizations) mainly to strip and compress the output files, which can be found [here](https://bitbucket.org/WillyPillow/jekyll-gulp).

Basically, it removes the spaces and newlines in the HTML/CSS files, deletes the CSS rules that are not in use, inlines the CSS style directly into the HTML to avoid another GET request, and compresses the images. It is also used to refresh the Cloudflare cache.

I originally wanted it to push the files onto my git repo, but it seemed that I couldn't use stdin with it, and I had to write an extra bash script to do the work.

## Github Pages (and Bitbucket)
Github Pages is what hosts this site. Though I prefer Bitbucket to Github, mainly because of habit, custom domains can't be used with Bitbucket Sites. As for [Aerobatic](https://www.aerobatic.com/), I like the whole user experience (being able to choose where _site/ lies directly rocks), but the 5 deployment per day limit for free users can be quite problematic since the comments here are static. However, since you have to put index.html right at the root directory of the repo, I have to create another branch/repo to host the Jekyll source files, and I chose Bitbucket for that task.

## Zoho Mail
Used to host emails for the domain. Thought of using [Works Mobile](https://www.worksmobile.com/jp/), but it seemed that the Chinese UI was gone. Besides, some said that the free plan had shut down.

## Namecheap
This is the only thing that I paid for regarding this site. Long story short, the process of changing anything else is transparent to the readers. On the other hand, if I use a free domain and they decide to shut me down for no reason (like what happened to willypillow.tk), I will have to find a way to redirect users to my new domain, which is, of course, rather difficult. The reason I use Namecheap instead of any other registar is that it supports Bitcoin. In case you don't know what Bitcoin is, it's a decentralized virtual currency, which means that no entity can manipulate it like what they did to fiat currency. Anyway, I'm a big fan of Bitcoin, thus I prefer to pay by that instead of other payment methods.

## Cloudflare
Now, I know some of you are complaining about how Cloudflare is bad. I've read a lot of critiques, but decided to use it anyway because of the speed boost it brings. In turn, I disabled most of the "checking your browser" and Captcha nonsense. However, they seem to be injecting a cookie called "__cfduid", possibly for analytics. If you're using [uMatrix](https://github.com/gorhill/uMatrix) (which, by the way, you should), you can block it easily. At the end of the day, you should just give Cloudflare the same level of trust as those of Github Pages, Namecheap, and even myself: minimal.

## Vim
Since Jekyll is pretty command-line oriented, it should come as no surprise that I also use a CLI editor for posting. In short, Vim is the best text editor I've ever used, period. It has high customizability, is minimal, and is light on resources. I usually pair it with Zsh and Tmux, running on Cygwin since my Asus T100TA laptop/tablet has bad Linux compatibility.

The Zsh + Tmux + Vim configuration that I use can be found [here](https://bitbucket.org/WillyPillow/consoleconf).

