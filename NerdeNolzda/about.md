---
layout: page
title: About
date: 2016-07-04 20:23:37 +0800
moddate: 2016-08-09 17:43:19 +0800
permalink: /about/
---
## What is this site about anyway?
In a nutshell, this is a blog mostly about computers, with a bit of other nerdy stuff.

## What does "Nerde Nolzda" mean?
Roughly "Geek Palace" in [lojban](https://mw.lojban.org/papri/Lojban). Now, I know that I shouldn't be using capitization like that in lojban, but it looks more consistant when used with English.

## Who are you?

### WillyPillow
Currently the one and only guy managing this place. I'm interested in programming, computer technology, Rubik's Cubes, anime, anarcho-captitalism, and so on. Coming from Kaohsiung, Taiwan, I'm able to speak Mandarin Chinese. Therefore, while this blog is mainly in English, I might write a few posts in Chinese if I ever have the need, under the tag of [zh](/tag/zh/), while posting other English articles under [en](/tag/en).

