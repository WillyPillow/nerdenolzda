---
layout: page
title: Commenting
date: 2016-07-15 07:18:52 +0800
moddate: 2016-08-09 17:43:19 +0800
permalink: /commenting/
---

As per [this post](https://nerde.pw/2016/07/04/hello-world.html#python-and-ruby), the commenting system on this site works by sending emails to a certain address. The specific information is shown below each post. Keep in mind that the title differs from post to post.

By default, the system will use the displayed name of your email account as the comment author.

If you want to publish by a certain name, write `name=YOURNAME` as the first line of the email body.

Since the system is not real-time, it may take a while for your comment to appear on the page. Please be patient and not double-post.

*NOTE: As for 2016-07-15, the automatic daemon is not running. Thus, the comments are being processed manually, and may take a day or two to appear.*

